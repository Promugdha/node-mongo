const assert = require('assert');

exports.insertDocument = (db, documents, collection) => {

    const coll = db.collection(collection);

    return coll.insert(documents);
};

exports.findDocuments = (db, collection) => {

    const coll = db.collection(collection);

    return coll.find({}).toArray();
};

exports.updateDocument = (db, update, documents, collection) => {

    const coll = db.collection(collection);

    return coll.updateOne(documents, { $set: update }, null);
};

exports.deleteDocument = (db, documents, collection) => {

    const coll = db.collection(collection);

    return coll.deleteOne(documents);
};