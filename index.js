const mongoClient = require('mongodb').MongoClient; //importing mongodb node module
const assert = require('assert');
const dbopr = require('./operations');                   //importing assert nm which checks for err

const url = "mongodb://localhost:27017/";
const dbname = "conFusion";

mongoClient.connect(url).then((client) => {          //connectiong to mongo db 

    console.log("connected to the server");
    const db = client.db(dbname);      //connecting to the db

    dbopr.insertDocument(db, { name: "burger", description: "big" }, "dishes")
        .then((result) => {
            console.log("inserted:\n", result.ops);
            return dbopr.findDocuments(db, "dishes")
        })
        .then((docs) => {
            console.log("the records are:\n", docs)
            return dbopr.updateDocument(db, { description: "very big" }, { name: "burger" }, "dishes")
        })
        .then((result) => {
            console.log("the updated data is:\n", result.result);
            return dbopr.findDocuments(db, "dishes")
        })
        .then((docs) => {
            console.log("the records are :\n", docs);
            return db.dropCollection("dishes")
        })
        .then((result) => {
            console.log("Dropped Collection: ", result);
            return client.close();
        })
        .catch((err) => console.log(err))
})
    .catch((err) => console.log(err));
